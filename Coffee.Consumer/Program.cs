﻿// See https://aka.ms/new-console-template for more information

using Confluent.Kafka;

var config = new ConsumerConfig
{
    BootstrapServers = "localhost:58395",
    GroupId = $"my-group",
    AutoOffsetReset = AutoOffsetReset.Earliest
};

using var consumer = new ConsumerBuilder<string, string>(config).Build();
consumer.Subscribe("quickstart");

while (true)
{
    var consumeResult = consumer.Consume(TimeSpan.FromSeconds(1));

    if (consumeResult is not null)
    {
        Console.WriteLine($"Consumed message: {consumeResult.Message.Value}");
    }
}