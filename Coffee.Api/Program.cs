using System.Collections;
using System.Collections.Concurrent;
using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;

const string DateFormat = "mm/dd/yy HH:MM:ss";

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<InMemoryStorage>();
builder.Services.AddHostedService<FlushService>();

var config = new ProducerConfig
{
    BootstrapServers = "localhost:58395"
};

builder.Services.AddSingleton<IProducer<string, string>>(_ => new ProducerBuilder<string, string>(config)
    .Build());

var app = builder.Build();

app.MapPost("/record-event", async ([FromBody] Event @event) =>
{
    var config = new ProducerConfig
    {
        BootstrapServers = "localhost:58395"
    };

    using (var producer = new ProducerBuilder<string, string>(config).Build())
    {
        var result = await producer.ProduceAsync("quickstart", new Message<string, string>
        {
            Value = @event.Message
        });

        return Results.Ok(result.Status.ToString());

    }
});

app.MapPost("record-event/v3", async ([FromBody] Event @event, [FromServices] IProducer<string, string> producer) =>
{
    var result = await producer.ProduceAsync("quickstart", new Message<string, string>
    {
        Value = @event.Message
    });
    
    return Results.Ok(result.Status.ToString());
});

app.MapPost("/record-event/v2", async ([FromBody] Event @event, [FromServices] InMemoryStorage storage) =>
{
    storage.Add(@event.Message);
    
    return Results.Ok();
});

app.Run();

public class InMemoryStorage
{
    private readonly Dictionary<string, ConcurrentQueue<string>> _storage = new();
    
    public InMemoryStorage()
    {
        // create buckets
        for (var i = 0; i <= 10; i++)
        {
            var key = DateTime.UtcNow.Subtract(TimeSpan.FromSeconds(i)).ToString("mm/dd/yy HH:MM:ss");
            _storage.TryAdd(key, new ConcurrentQueue<string>());
        }
        for (var i = 0; i <= 300; i++)
        {
            var key = DateTime.UtcNow.Add(TimeSpan.FromSeconds(i)).ToString("mm/dd/yy HH:MM:ss");
            _storage.TryAdd(key, new ConcurrentQueue<string>());
        }
    }

    public void Add(string message)
    {
        var key = DateTime.UtcNow.ToString("mm/dd/yy HH:MM:ss");
        
        _storage[key].Enqueue(message);
        
        Console.WriteLine($"{message} - added to bucket {key}");
    }

    public ConcurrentQueue<string> Get(string key)
    {
        return _storage[key];
    }
}

public class FlushService : BackgroundService 
{
    private readonly IServiceProvider _serviceProvider;

    public FlushService(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            using var scope = _serviceProvider.CreateScope();
            var storage = scope.ServiceProvider.GetRequiredService<InMemoryStorage>();
            var key = DateTime.UtcNow.Subtract(TimeSpan.FromSeconds(1)).ToString("mm/dd/yy HH:MM:ss");
            
            var config = new ProducerConfig
            {
                BootstrapServers = "localhost:58395"
            };

            using (var producer = new ProducerBuilder<string, string>(config).Build())
            {
                Console.WriteLine($"----Flushing {key}");

                foreach (var msg in storage.Get(key))
                {
                    Console.WriteLine(msg);

                    var result = await producer.ProduceAsync("quickstart", new Message<string, string>
                    {
                        Value = msg
                    });
                }
            }

            Console.WriteLine($"----Flushed {key}");
            
            await Task.Delay(TimeSpan.FromSeconds(1), stoppingToken);
        }
    }
}

public record Event {
    public string Message { get; set; }
}