﻿using System.Text;
using System.Text.Json;
using NBomber.CSharp;
using NBomber.Http.CSharp;

using var httpClient = new HttpClient();

var scenario = Scenario.Create("hello_world_scenario", async context =>
    {
        var request =
            Http.CreateRequest("POST", "http://localhost:5269/record-event/v3")
                .WithHeader("Accept", "text/html")
                .WithBody(new StringContent(JsonSerializer.Serialize(new Event()), Encoding.UTF8, "application/json"));
        
        var response = await Http.Send(httpClient, request);

        return response;
    })
    .WithoutWarmUp()
    .WithLoadSimulations(
        Simulation.Inject(rate: 10000,
            interval: TimeSpan.FromSeconds(1),
            during: TimeSpan.FromSeconds(5))
    );

NBomberRunner
    .RegisterScenarios(scenario)
    .Run();

public record Event
{
    public string Message { get; init; } = Guid.NewGuid().ToString();
}